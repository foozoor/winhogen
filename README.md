## <samp>OVERVIEW</samp>

Opinionated post-installation script for Windows 11.

<a href="../.." target="_blank"><img src="https://raw.githubusercontent.com/sharpordie/mybadges/main/src/kofi.svg"></a>

## <samp>GUIDANCE</samp>

### Launch latest script release

```powershell
$address = "https://codeberg.org/foozoor/winhogen/raw/branch/main/src/winhogen.ps1"
$fetched = ni $env:temp\winhogen.ps1 -f ; iwr $address -o $fetched
try { pwsh -ep bypass $fetched } catch { powershell -ep bypass $fetched }
```